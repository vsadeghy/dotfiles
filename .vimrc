let mapleader = ","

set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-surround'
Plugin 'vimwiki/vimwiki'
Plugin 'preservim/nerdtree'
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'
Plugin 'ryanoasis/vim-devicons'
Plugin 'vim-python/python-syntax'
Plugin 'lukesmithxyz/vimling'
Plugin 'kovetskiy/sxhkd-vim'
Plugin 'tpope/vim-commentary'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'junegunn/vim-emoji'
Plugin 'vifm/vifm.vim'
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'}  "Fuzzy finder
Plugin 'junegunn/fzf.vim'
Plugin 'Yggdroot/indentLine'

call vundle#end()

filetype plugin indent on
syntax on
set number
set autoindent
set relativenumber
set go=a
set mouse=a
set nohlsearch
set incsearch
set clipboard+=unnamedplus
set wildmode=longest,list,full
set backspace=indent,eol,start
set hidden
set t_Co=256
set laststatus=2
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" NERDTree
	map <leader>n :NERDTreeToggle<CR>
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
	let g:NERDTreeDirArrowExpandable = '►'
	let g:NERDTreeDirArrowCollapsible = '▼'
	let NERDTreeShowLineNumbers=1
	let NERDTreeShowHidden=1
	let NERDTreeMinimalUI = 1
	let g:NERDTreeWinSize=38

"Goyo plugin makes text more readable when writing prose:
    map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

" vimling:
	nm <leader>d :call ToggleDeadKeys()<CR>
	imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
	nm <leader>i :call ToggleIPA()<CR>
	imap <leader>i <esc>:call ToggleIPA()<CR>a
	nm <leader>q :call ToggleProse()<CR>

" Shortcutting split navigation, saving a keypress:
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" Move line
    map <M-Down> <esc>ddp
    map <M-Up> <esc>ddkP
" Replace ex mode with gq
	map Q gq

" Ensure files are read as what I want:
	let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
	map <leader>v :VimwikiIndex
	let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
	autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
	autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
	autocmd BufRead,BufNewFile *.tex set filetype=tex

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Enable Goyo by default for mutt writting
    autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
    autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=light
    autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
    autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" Update binds when sxhkdrc is updated.
	autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

if &diff
    highlight! link DiffText MatchParen
endif

set noshowmode
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

set fillchars+=vert:\
