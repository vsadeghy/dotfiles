# Use neovim for vim if present.
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	bat="cat /sys/class/power_supply/BAT?/capacity" \
	cp="cp -i" \
	mv="mv -i" \
	rm="rm -i" \
	mkd="mkdir -pv" \
    config="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME" \
    pg="ping google.com" \

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
    s="sudo" \
	trem="transmission-remote" \
	YT="youtube-viewer" \
	sdn="sudo shutdown -h now" \
	f="$FILE" \
	e="$EDITOR" \
	v="$EDITOR" \
	p="sudo pacman" \
	pi="sudo pacman -S" \
	pr="sudo pacman -R" \
    yi="yay -S" \
	xi="sudo xbps-install" \
	xr="sudo xbps-remove -R" \
	xq="xbps-query" \
    colorscript="/opt/shell-color-scripts/colorscript.sh" \


# Some other stuff
alias \
	ac="vim ~/.config/aliasrc" \
	zc="vim ~/.zshrc" \
	vc="vim ~/.vimrc" \
	nvc="vim ~/.config/nvim/init.vim" \
    sxc="vim ~/.config/sxhkd/sxhkdrc" \
    bsc="vim ~/.config/bspwm/bspwmrc" \
    pic="vim ~/.config/picom/picom.conf" \
    xc="vim ~/.xinitrc" \
	rr=!! \
	vim=nvim \

#Changing "ls" to "exa"
alias \
	ls="exa -al --color=always --group-directories-first --icons" \
	la="exa -a --color=always --group-directories-first --icons" \
	ll="exa -l --color=always --group-directories-first --icons" \
	lt="exa -aT --color=always --group-directories-first --icons" \
    lf='find . -maxdepth 1 -type d' \
    fd='find . -type d -name' \
    ff='find . -type f -name' \
