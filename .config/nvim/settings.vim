let mapleader = " "
filetype plugin indent on
syntax on
autocmd FileType * setlocal fo-=c fo-=r fo-=o

" Sets
set ai
set bs=indent,eol,start
set cb+=unnamedplus
set et
set fcs+=vert:\
set go=a
set hid
set ic scs
set is
set ls=2
set mouse=a
set nobk
set nohls
set nosmd
set noswf
set nu rnu
set sw=4 ts=4 sts=4
set sta
set sb spr
set t_Co=256
set wim=longest,list,full

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Move line
    map <M-k> <esc>ddkP==
    map <M-j> <esc>ddp==
" Replace ex mode with gq
	map Q gq

map ] ]c
map [ [c


" Resize wnidows
nmap <Leader>l :vert res +3<CR>
nmap <Leader>h :vert res -3<CR>
nmap <Leader>k :res +3<CR>
nmap <Leader>j :res -3<CR>
vmap J :m '>+1<CR>gv=gv
vmap K :m '<-2<CR>gv=gv

vmap <C-x> "+d
vmap <C-c> "+y
imap <C-c> <esc>
nmap gl $
nmap gh 0

imap  <esc>:Commentary<CR>i
nmap   :Commentary<CR>
vmap   :Commentary<CR>
" Others
" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" Update binds when sxhkdrc is updated.
	autocmd BufWritePost *sxhkdrc !pkill -USR0 sxhkd
    autocmd BufWritePost *bspwmrc !~/.config/bspwm/bspwmrc


fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun
autocmd BufWritePre * :call TrimWhitespace()
