source ~/.config/nvim/plugins.vim
source ~/.config/nvim/settings.vim
source ~/.config/nvim/plugconf/nerdtree.vim
"source ~/.config/nvim/plugconf/vimwiki.vim
source ~/.config/nvim/plugconf/vimling.vim
source ~/.config/nvim/plugconf/goyo.vim
source ~/.config/nvim/plugconf/coc.vim
source ~/.config/nvim/plugconf/quickscope.vim
source ~/.config/nvim/plugconf/rainbow.vim
source ~/.config/nvim/plugconf/fzf.vim
source ~/.config/nvim/plugconf/airline.vim
source ~/.config/nvim/plugconf/prettier.vim

nmap <leader>u :UndotreeToggle<CR>

if &diff
    highlight! link DiffText MatchParen
endif

"autocmd FileType js,ts,cpp,cxx,h,hpp,c :call GoCoc()
let g:molokai_original = 1
let g:rehash256 = 1
let g:vimwiki_conceallevel = 2
