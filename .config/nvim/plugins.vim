set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'christianchiarulli/onedark.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'jiangmiao/auto-pairs'
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'}  "Fuzzy finder
Plugin 'junegunn/fzf.vim'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
Plugin 'junegunn/rainbow_parentheses.vim'
Plugin 'junegunn/vim-emoji'
Plugin 'justinmk/vim-sneak'
Plugin 'kevinhwang91/rnvimr', {'do': 'make sync'}
Plugin 'kovetskiy/sxhkd-vim'
" Plugin 'liuchengxu/vim-which-key'
Plugin 'lukesmithxyz/vimling'
Plugin 'mbbill/undotree'
Plugin 'neoclide/coc.nvim', {'branch': 'release'}
Plugin 'preservim/nerdtree'
Plugin 'rainglow/vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'sheerun/vim-polyglot'
Plugin 'ThePrimeagen/vim-be-good'
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'unblevable/quick-scope'
Plugin 'vifm/vifm.vim'
Plugin 'vim-python/python-syntax'
Plugin 'vimwiki/vimwiki'
"Plugin 'ycm-core/YouCompleteMe'

call vundle#end()
